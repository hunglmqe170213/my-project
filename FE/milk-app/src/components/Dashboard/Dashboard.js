import React from 'react';
import PropTypes from 'prop-types';

function Dashboard({ setToken }) {
  const handleLogout = () => {
    // Xóa token khỏi localStorage (đăng xuất)
    localStorage.removeItem('token');
    // Điều hướng người dùng về trang Login
    window.location.href = '/login'; // Thay đổi đường dẫn tùy theo cấu hình của bạn
  };

  return (
    <div>
      <h1>Welcome to Dashboard</h1>
      <button onClick={handleLogout}>Logout</button>
    </div>
  );
}

Dashboard.propTypes = {
  setToken: PropTypes.func.isRequired
};

export default Dashboard;
